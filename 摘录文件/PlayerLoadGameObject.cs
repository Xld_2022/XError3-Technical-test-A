using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System;

public class PlayerLoadGameObject : MonoBehaviour
{
    // 通用的对象加载方法
    public void LoadGameObject(string addressableKey, Action<GameObject> onSuccess, Action<string> onError)
    {
        // 使用Addressables异步加载GameObject
        Addressables.LoadAssetAsync<GameObject>(addressableKey).Completed += (AsyncOperationHandle<GameObject> obj) =>
        {
            if (obj.Status == AsyncOperationStatus.Succeeded)
            {
                GameObject loadedObject = obj.Result;
                if (loadedObject != null)
                {
                    // 实例化加载的对象
                    GameObject instance = Instantiate(loadedObject);
                    onSuccess?.Invoke(instance); // 调用成功回调，传递实例化的对象
                }
                else
                {
                    onError?.Invoke("Loaded GameObject is null."); // 调用错误回调
                }
            }
            else
            {
                onError?.Invoke($"Failed to load GameObject from Addressables: {obj.OperationException}"); // 调用错误回调
            }
        };
    }

    // 通用的对象卸载方法
    public void UnloadGameObject(GameObject instance)
    {
        if (instance != null)
        {
            // 销毁实例化的对象
            Destroy(instance);
            // 释放Addressables资源
            Addressables.ReleaseInstance(instance);
        }
    }
    
    // 通用的纹理加载方法
    public void LoadTexture(string addressableKey, Action<Texture2D> onSuccess, Action<string> onError)
    {
        Addressables.LoadAssetAsync<Texture2D>(addressableKey).Completed += (AsyncOperationHandle<Texture2D> obj) =>
        {
            if (obj.Status == AsyncOperationStatus.Succeeded)
            {
                Texture2D texture = obj.Result;
                if (texture != null)
                {
                    onSuccess?.Invoke(texture);
                }
                else
                {
                    onError?.Invoke("Loaded texture is null.");
                }
            }
            else
            {
                onError?.Invoke($"Failed to load texture from Addressables: {obj.OperationException}");
            }
        };
    }
}