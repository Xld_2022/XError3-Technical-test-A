using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.IO;
using Newtonsoft.Json;

// 皮肤类型结构体
public class Skin
{
    public string name;
    public string Type;
}
public class SkinTypeArray
{
    public Skin[] skinType;
}


public class Player : MonoBehaviour
{
    // 参数
    public SkinTypeArray indexesSkinType;
    private bool indexesSkinType_state; // indexesSkinType加载状态
    
    public float walkSpeed; // 行走速度，表示角色正常行走时的移动速度。
    public float runSpeed; // 奔跑速度，表示角色按下跑步键时的移动速度。
    public float crouchSpeed = 3.0f; // 潜行速度，表示角色在潜行（蹲下）状态下的移动速度。
    public float jumpForce = 5.0f; // 跳跃力，表示角色跳跃时施加的向上力的大小。
    public float crouchHeight = 1.2f; // 潜行高度，表示角色在潜行（蹲下）状态时的胶囊体碰撞器的高度。
    public float standingHeight = 1.6f; // 站立高度，表示角色在站立状态时的胶囊体碰撞器的高度。
    public float calibrationHeight = 0.43f; // 中心偏移位置，用来校准刚体中心位置（模型问题，没办法）
    public float gravity = -9.81f; // 重力

    
    
    
    // 实体类型
    // 皮肤
    public string skinType;
    private string skinFilPath;
    private string skinPngFilePath; // PNG图片的路径
    private string[] skinChildObjectName = {"SimplePlayer.Body.Layer1","SimplePlayer.Body.Layer2"}; // 更改材质对象
    public string skinName; // 皮肤名字
    // 对象
    public string PlayerObjectType;
    public GameObject PlayerObject;
    
    // 对象状态
    public CharacterController controller;
    private Vector3 velocity;
    public bool isGrounded;
    public bool isCrouching;
    
    // 对象控制
    public Transform HeadTransform; // 头部Transform
    
    // 锁
    public bool Lock_Move; // 移动
    public bool Lock_Crouch; // 蹲起
    public bool Lock_Jump; // 跳跃
    public bool Lock_esc; // esc锁
    
    
    // 其他代码
    private PlayerLoadGameObject addressablesLoader; // 玩家主体加载函数库
    public PlayerPermissions playerPms; // 玩家权限控制库
    public PlayerCamera playerCamera; // 玩家相机
    public PlayerAnimatorControl playerAnimatorControl; // 玩家动画控制器
    
    void Start()
    {
        // 测验代码
        
        // 初始化
        StartCoroutine(initialize_Main());
        
        controller = GetComponent<CharacterController>();
        controller.height = standingHeight;
        
        controller.center = new Vector3(0, standingHeight / 2 + calibrationHeight, 0);
        
        // 获取PlayerPermissions组件
        playerPms = GetComponent<PlayerPermissions>();
    }

    void Update()
    {
        // 检测是否在地面上
        isGrounded = controller.isGrounded;
        
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f; // 确保角色贴在地面上
        }
        
        if (Lock_Crouch && Lock_esc)
        {
            // 检测蹲下状态
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                Crouch();
            }

            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                StandUp();
            }
        }

        // 处理跳跃输入
        if (isGrounded && Input.GetButtonDown("Jump") && 
            Lock_Jump && Lock_esc)
        {
            Jump();
        }

        if (Lock_Move && Lock_esc)
        {
            Move(isGrounded);
        }
        
        // 头部控制
        if (HeadTransform != null)
        {
            HeadTransform.rotation = playerCamera.xRotationObject.rotation;
        }
    }
    
    // 移动交互模块
    private void Move(bool isGrounded)
    {
        // 根据当前状态选择移动速度
        float moveSpeed = walkSpeed;
        /* 作者懒，说以就物理禁用跑步了 OwO/
         if (Input.GetKey(KeyCode.LeftControl) && !isCrouching)
         {
             moveSpeed = runSpeed;
         }
         if (isCrouching)
         {
            moveSpeed = crouchSpeed;
         } */

        // 获取玩家输入
        float moveX = Input.GetAxis("Horizontal");
        float moveZ = Input.GetAxis("Vertical");
        
        // 判断玩家是否在移动
        bool isMoving = moveX != 0 || moveZ != 0;

        if (isMoving)
        {
            playerAnimatorControl.State_Move = true;
        }
        else
        {
            playerAnimatorControl.State_Move = false;
        }

        // 计算移动方向
        Vector3 move = transform.right * moveX + transform.forward * moveZ;

        // 应用移动
        controller.Move(move * moveSpeed * Time.deltaTime);

        // 应用重力
        if (!isGrounded)
        {
            velocity.y += gravity * Time.deltaTime;
        }

        // 应用垂直速度
        controller.Move(velocity * Time.deltaTime);
    }

    // 跳跃函数
    private void Jump()
    {
        velocity.y = Mathf.Sqrt(jumpForce * -2f * gravity);
        // Debug.Log("Jump: " + velocity.y);
    }

    // 蹲下函数
    private void Crouch()
    {
        isCrouching = true;
        controller.height = crouchHeight;
        controller.center = new Vector3(0, crouchHeight / 2 + calibrationHeight, 0);
    }

    // 站起函数
    private void StandUp()
    {
        if (!Physics.SphereCast(transform.position, controller.radius, Vector3.up, out RaycastHit hit, standingHeight - crouchHeight))
        {
            isCrouching = false;
            controller.height = standingHeight;
            controller.center = new Vector3(0, standingHeight / 2 + calibrationHeight, 0);
        }
    }
    
    
    
    
    
    // 初始模块
    // 数据加载初始/主初始化
    private IEnumerator initialize_Main()
    {
        // 上锁，禁止玩家移动、跳跃、潜行等操作
        Lock_Crouch = Lock_Jump = Lock_Move = false;

        // 玩家模型初始化
        initialize_PlayerData();

        // 等待玩家数据初始化完成
        while (!indexesSkinType_state)
        {
            yield return null; // 每帧检查一次条件，直到初始化完成
        }

        // 玩家数据查询和处理
        initialiiz_PlayerDataProcessing(skinType, skinName);

        // 对象加载，并等待加载完成
        bool isLoadingComplete = false;
        addressablesLoader = gameObject.AddComponent<PlayerLoadGameObject>();
        addressablesLoader.LoadGameObject(
            $"PlayerObject/{PlayerObjectType}模型.prefab",
            (loadedObject) =>
            {
                OnCharacterLoaded(loadedObject);
                isLoadingComplete = true;
            },
            OnError
        );

        // 等待对象加载完成
        while (!isLoadingComplete)
        {
            yield return null; // 每帧检查一次加载状态
        }

        // 装载动画模块
        playerAnimatorControl = PlayerObject.GetComponent<PlayerAnimatorControl>();
        
        // 装载头部
        HeadTransform = PlayerObject.transform.Find("SimplePlayer.arma/MAIN/center/Body/Chest/Head");

        // 解锁，允许玩家移动和跳跃
        Lock_Move = true;
        Lock_Jump = true;

        // 解锁摄像机旋转
        playerCamera.Lock_RotateX = playerCamera.Lock_RotateY = true;

        // 退出协程
        yield return null; // 确保协程在最后一帧正常退出
    }
    
    // 玩家数据加载
    public void initialize_PlayerData()
    {
        // 皮肤注册表加载方法
        // 索引地址
        string indexesFilePath = "Assets/player/自带皮肤/indexes.json";
        // 加载JSON文件
        indexesSkinType_state = false;
        LoadJsonFile<SkinTypeArray>(indexesFilePath, OnSuccess, OnError);
    }
    
    // 数据查询/操作
    public void initialiiz_PlayerDataProcessing(string type, string skinFilePath, bool skinOuterIO = false)
    {
        if (!skinOuterIO)
        {
            // 查询本地皮肤
            if (indexesSkinType != null)
            {
                foreach (Skin skinType in indexesSkinType.skinType)
                {
                    if (skinType.name == skinFilePath)
                    {
                        if (skinType.Type != "universal")
                        {
                            PlayerObjectType = skinType.Type;
                        }

                        break;
                    }
                }
            }
            else
            {
                PlayerObjectType = "Alex";
            }
        }
        else
        {
            if (PlayerObjectType != "Alex" && PlayerObjectType != "Steve")
            {
                PlayerObjectType = "Alex";
            }
            // 外部加载
        }
    }



    // 通用的JSON加载方法 (Addressables地址,成功调用,失败调用)
    // 通用的JSON加载方法
    public void LoadJsonFile<T>(string addressableKey, Action<T> onSuccess, Action<string> onError)
    {
        // 使用Addressables异步加载TextAsset
        Addressables.LoadAssetAsync<TextAsset>(addressableKey).Completed += (AsyncOperationHandle<TextAsset> obj) =>
        {
            // 检查加载结果
            if (obj.Status == AsyncOperationStatus.Succeeded)
            {
                TextAsset jsonFile = obj.Result; // 获取加载的TextAsset
                if (jsonFile != null)
                {
                    try
                    {
                        // 使用Newtonsoft.Json解析JSON内容
                        T data = JsonConvert.DeserializeObject<T>(jsonFile.text);
                        onSuccess?.Invoke(data); // 调用成功回调，传递解析后的数据
                    }
                    catch (Exception e)
                    {
                        // 捕获解析过程中可能出现的异常，并调用错误回调
                        onError?.Invoke($"Failed to parse JSON file: {e.Message}");
                    }
                }
                else
                {
                    // 如果加载的TextAsset为空，调用错误回调
                    onError?.Invoke("Failed to load JSON file content.");
                }
            }
            else
            {
                // 如果加载失败，调用错误回调
                onError?.Invoke($"Failed to load JSON file from Addressables: {obj.OperationException}");
            }
        };
    }

    // 成功加载和解析JSON文件的通用回调方法
    void OnSuccess<T>(T data)
    {
        if (data is SkinTypeArray skinTypeArray)
        {
            indexesSkinType = data as SkinTypeArray;
            indexesSkinType_state = true;
        }
    }
    
    // 成功加载和实例化游戏人物的回调方法
    void OnCharacterLoaded(GameObject character)
    {
        PlayerObject = character;
        PlayerObject.transform.SetParent(transform);
        PlayerObject.transform.position = Vector3.zero;
        // Debug.Log("Character loaded and instantiated.");
        PlayerObject.transform.localPosition = new Vector3(0,0.42f,0); // 设置加载对象的位置
        PlayerObject.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
        // Debug.Log("Character loaded and instantiated at position: " + PlayerObject);

        // 加载纹理
        addressablesLoader.LoadTexture($"Assets/player/自带皮肤/{skinName}.png", OnTextureLoaded, OnError);
    }

    // 加载或解析JSON文件失败的回调方法
    void OnError(string error)
    {
        // 打印错误信息到控制台
        Debug.LogError(error);
    }
    
    
    
    void OnTextureLoaded(Texture2D texture)
    {
        StartCoroutine(ApplyTextureToChildren(texture, skinChildObjectName));
    }

    // 协程：应用纹理到指定子级的材质 更改平滑度
    IEnumerator ApplyTextureToChildren(Texture2D texture, string[] childNames/*, float smoothness*/)
    {
        yield return null;

        foreach (string childName in childNames)
        {
            Transform childTransform = PlayerObject.transform.Find(childName);
            if (childTransform != null)
            {
                Renderer childRenderer = childTransform.GetComponent<Renderer>();
                if (childRenderer != null)
                {
                    Material newMaterial = new Material(childRenderer.material);
                    newMaterial.mainTexture = texture;
                    // newMaterial.SetFloat("_Glossiness", smoothness); // 设置平滑度
                    childRenderer.material = newMaterial;

                    // Debug.Log($"Changed material texture of {childName} to loaded texture and set smoothness to {smoothness}");
                }
                else
                {
                    Debug.LogError($"Renderer component not found on {childName}");
                }
            }
            else
            {
                Debug.LogError($"Child object {childName} not found");
            }
        }
    }
}
