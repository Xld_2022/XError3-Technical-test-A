using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI_EscUI : MonoBehaviour
{
    public GameObject EscUIObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    // 在需要退出游戏的地方调用此方法
    public void QuitGame()
    {
        #if UNITY_EDITOR
                // 如果是在 Unity 编辑器中运行，停止播放
                UnityEditor.EditorApplication.isPlaying = false;
        #else
                // 如果是在打包后的应用程序中运行，退出应用
                Application.Quit();
        #endif
    }
    
    // 启用对象
    public void EnableObject()
    {
        if (EscUIObject != null)
        {
            EscUIObject.SetActive(true);
            Debug.Log(EscUIObject.name + " is now enabled.");
        }
    }

    // 禁用对象
    public void DisableObject()
    {
        if (EscUIObject != null)
        {
            EscUIObject.SetActive(false);
            Debug.Log(EscUIObject.name + " is now disabled.");
        }
    }
    
    
    
    // 函数：将文本复制到剪贴板
    public void CopyToClipboard(string text)
    {
        // 使用 GUIUtility 将文本写入系统剪贴板
        GUIUtility.systemCopyBuffer = text;
        Debug.Log("Copied to clipboard: " + text);
    }
}
