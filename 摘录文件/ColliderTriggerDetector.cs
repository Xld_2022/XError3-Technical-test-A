using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderTriggerDetector : MonoBehaviour
{
    public GameObject triggeredObject;
    // 碰撞箱式触发器通用传输代码
    void OnTriggerEnter(Collider other)
    {
        // 获取触发碰撞箱的对象的GameObject
        triggeredObject = other.gameObject;

        // 输出对象的名字，确认触发的对象
        // Debug.Log("Object that triggered the collider: " + triggeredObject.name);
    }
}
