using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public float mouseSensitivity; // 鼠标灵敏度
    public Transform xRotationObject; // 受控于X轴旋转的对象
    public Transform yRotationObject; // 受控于Y轴旋转的对象

    public float maxRotationSpeed; // 最大旋转速度（度/秒）

    public float minXRotation; // X轴旋转的最小角度
    public float maxXRotation; // X轴旋转的最大角度

    private float xRotation = 0f; // 用于存储X轴上的旋转角度
    
    
    public PlayerPermissions playerPms; // 玩家权限控制库
    
    // 控制锁
    public bool Lock_RotateX;
    public bool Lock_RotateY;
    public bool Lock_esc; // esc锁

    void Start()
    {
        
    }

    void Update()
    {
        // 获取鼠标移动的输入
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        // 计算新的旋转角度
        xRotation -= mouseY;

        // 限制X轴旋转角度
        xRotation = Mathf.Clamp(xRotation, minXRotation, maxXRotation);

        if (Lock_RotateX && Lock_esc)
        {
            // 应用X轴旋转到XRotationObject
            if (xRotationObject != null)
            {
                xRotationObject.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            }
        }


        if (Lock_RotateY && Lock_esc)
        {
            // 应用Y轴旋转到YRotationObject，不限制角度
            if (yRotationObject != null)
            {
                yRotationObject.Rotate(Vector3.up * mouseX, Space.World);
            }
        }
    }
}
